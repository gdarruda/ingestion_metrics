# Métricas de ingestão #

Script para geração de totalizadores referentes a uma tabela HIVE passada de parâmetro.

## Geração do .jar ##

Para geração do jar executável via spark submit, é necessário executar o comando assembly do sbt na raiz do projeto.

## Execução local ##

Para execução local, há um exemplo no arquivo LocalRun.scala. É necessário definir um local para armazenar o banco de dados local (hive.metastore.warehouse.dir) e definir o schema das tabelas através de DDLs antes de executar o levantamento de estatísticas:

~~~scala
hiveContext.setConf("hive.metastore.warehouse.dir", "file:/home/garruda/Projects/HiveDevelopment")
hiveContext.sql("CREATE TABLE IF NOT EXISTS teste_tabela_fixa(identificador INT, nome STRING, renda DECIMAL(6,2), data_inclusao TIMESTAMP)")
~~~

As bibliotecas também devem ser importadas localmente, removendo-se o atributo "provided" das bibliotecas Spark:

~~~scala
libraryDependencies += "org.apache.spark" %% "spark-core" % "1.6.1"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "1.6.1"
libraryDependencies += "org.apache.spark" %% "spark-hive" % "1.6.1"
~~~

## Execução no Spark ##

Para execução do processo dentro do Spark, são necessários dois parâmetros: tabela de origem e diretório de destino. Abaixo, uma chamada de exemplo:

~~~
spark-submit \
    --class Run \
    --master yarn-client \
    --driver-memory 2G \
    --executor-memory 16G \
    --num-executors 16 \
    --queue semantix \
    IngestionMetrics-assembly-1.0.jar DB_BANCO_EXEMPLO.TABELA_EXEMPLO /stats/DB_BANCO_EXEMPLO.TABELA_EXEMPLO
~~~

## Execução por script Python ##

Para extração de totalizadores de um lote de tabelas, há um script Python (validate_data.py) que executa o script para uma lista de tabelas. No diretório base definido no arquivo de configuração, serão criados sub-diretórios com os nomes das tabelas listadas e seus respectivos totalizadores. Para utilizar esse script, é necessário definir as seguintes configurações dentro de uma arquivo validate.cfg no diretório de execução:

~~~cfg
[validate]
#Database name
database_name=DB_BGDT_BANCO
#File containing the table list
path_table_list=table_list.txt
#Base directory for execution
path_validate=/stats/DB_BGDT_BANCO

~~~

Além do arquivo validate.cfg, é necessário ter o programa IngestionMetrics-assembly-1.0.jar e o diretório local /logs para armazenar as saídas da execução. O script é executado da seguinte forma:

~~~
mkdir -p logs
python import_data.py
~~~