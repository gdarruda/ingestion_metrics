import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object LocalRun {

  def main(args: Array[String]): Unit = {

    val sparkConf = new SparkConf()
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .setMaster("local[*]")
      .setAppName("IngestionMetrics")

    val spark = SparkSession
      .builder
      .config(sparkConf)
      .enableHiveSupport()
      .getOrCreate()

    spark.sqlContext.setConf("hive.metastore.warehouse.dir", "file:/home/garruda/Projects/HiveDevelopment")
    spark.sqlContext.sql("CREATE TABLE IF NOT EXISTS teste_tabela_fixa(identificador INT, nome STRING, renda DECIMAL(6,2), data_inclusao TIMESTAMP)")

    val aggregations = Array("MAX", "MIN", "COUNT")
    val tableName = "teste_tabela_fixa"
    val tableDefinition = spark.sqlContext.sql(s"DESCRIBE $tableName")

    tableDefinition.printSchema()
    tableDefinition.show()

    val queryBuilder = new QueryBuilder(aggregations)
    val statsQuery = queryBuilder.getQuery(tableDefinition, tableName)

    spark.sqlContext
      .sql(statsQuery)
      .show()

  }

}