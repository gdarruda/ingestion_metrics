import org.apache.spark.sql.DataFrame

class QueryBuilder(aggregations: Array[String]) extends Serializable{

  def getAggregations(columnName: String, columnType: String, aggregations: Array[String]) : String = {

    aggregations
      .map(aggregation => {

        columnType match {
          case "timestamp" => s"'$aggregation($columnName)', $aggregation(CAST($columnName AS STRING))"
          case "date"  => s"'$aggregation($columnName)', $aggregation(CAST($columnName AS STRING))"
          case _ => s"'$aggregation($columnName)', $aggregation($columnName)"
        }

      })
      .reduce((column1, column2) => s"$column1 ,\n $column2")

  }


  def getProjection(tableDefinition: DataFrame) : String = {

    "map('COUNT(*)', COUNT(*), \n" + tableDefinition
      .collect()
      .map(x => getAggregations(x.getAs("col_name"), x.getAs("data_type"), aggregations))
      .reduce((column1, column2) => s"$column1 ,\n $column2") + ") as aggregations"

  }

  def getQuery(tableDefinition: DataFrame, tableName: String) = {

    val projection = getProjection(tableDefinition)

    s"""SELECT '$tableName' AS table_name,
       |        aggreg_type,
       |        aggreg_value
       |FROM (SELECT $projection
       |      FROM   $tableName) x
       |LATERAL VIEW explode(aggregations) toInline as aggreg_type, aggreg_value""".stripMargin
  }

}
