import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.slf4j.LoggerFactory

object Run {
  def main(args: Array[String]): Unit = {

    val sparkConf = new SparkConf()
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .setAppName("IngestionMetrics")

    val (databaseName, targetTable) = (args(0), args(1))
    val spark = SparkSession
      .builder()
      .appName("MetricasHive")
      .enableHiveSupport()
      .getOrCreate()

    import spark.implicits._

    val aggregations = Array("MAX", "MIN", "COUNT")

    val logger = LoggerFactory.getLogger(Run.getClass)


    val tablesInDatabase = spark.sqlContext.sql(s"SHOW TABLES IN $databaseName")
        .filter(!$"isTemporary")
        .collect()

    tablesInDatabase
      .foreach(tableInfo => {

        val tableName = s"$databaseName.${tableInfo.getString(1)}"

        val tableDefinition = spark.sqlContext.sql(s"DESCRIBE $tableName")
        val queryBuilder = new QueryBuilder(aggregations)
        val statsQuery = queryBuilder.getQuery(tableDefinition, tableName)

        spark.sqlContext
          .sql(statsQuery)
          .write
          .insertInto(targetTable)

      })

    spark.close()

  }
}