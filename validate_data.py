#coding=utf-8
import csv
import os
from subprocess import call
import glob
from ConfigParser import SafeConfigParser

def get_execution_config():

	config = SafeConfigParser()
	try:
		config.read("validate.cfg")
		database_name = config.get("validate","database_name").strip()
		path_table_list = config.get("validate","path_table_list").strip()
		path_validate =  config.get("validate","path_validate").strip()
		return (path_table_list, database_name, path_validate)
	except Exception as e:
		print "Erro ao ler o arquivo de configurações:" + str(e)
		print "Certifique-se que o import.cfg está na pasta de instalação do pacote,"
		print "e que suas seções estejam devidamente configuradas, conforme documentação."
		exit(1)

def read_table_list(path_table_list):
	configs = []
	with open(path_table_list, 'r') as csvfile:
		reader = csv.reader(csvfile, delimiter=",")
		for config in reader:
			configs.append(config)
	return configs

def get_spark_command(validation_path, table_name, database_name):
	command = []
	command.append("spark-submit")

	command.append("--class")
	command.append("Run")

	command.append("--master")
	command.append("yarn-client")

	command.append("--driver-memory")
	command.append("2G")

	command.append("--num-executors")
	command.append("16")

	command.append("--executor-memory")
	command.append("16G")

	command.append("--queue")
	command.append("semantix")

	command.append("IngestionMetrics-assembly-1.0.jar")

	command.append(database_name + "." + table_name)
	command.append(validation_path + "/" + table_name)

	return command

def validate_data(path_table_list, database_name, path_validate):

	print "Iniciando validação do banco de dados " + database_name + "... \n"

	try:
		table_list = read_table_list(path_table_list)
	except Exception as e:
		print "Falha ao ler o arquivo de configurações. Mensagem: " + str(e)
		exit(1)

	if table_list and len(table_list) > 0:
		for table in table_list:

			table_name = table[0].strip()

			command = get_spark_command(path_validate, table_name, database_name)

			print(command)

			try:
				print "Gerando totalizadores: " + table_name + "..."
				p = call(command,stderr=open("logs/stderr_validate-"+ table_name+".log","w"), stdout=open("logs/stdout_validate-"+table_name+".log","w"))
				if p == 0:
					print "Validacao concluído com sucesso."
				else:
					print "Falha durante o processo de validação da tabela: " + table_name + "."
					print "Favor checar o arquivo stderr_parser.log, ou enviá-lo para a Semantix."
					exit(1)
				print "======================================================\n"
			except Exception as e:
				print "Erro: " + str(e)
				exit(1)
	else:
		print "O arquivo de configuração não foi preenchido corretamente."


if __name__ == "__main__":
	path_table_list, database_name, path_validate = get_execution_config()
	validate_data(path_table_list, database_name, path_validate)
